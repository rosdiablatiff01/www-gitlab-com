---
layout: markdown_page
title: "Diversity and Inclusion Events"
---

## On this page
{:.no_toc}

- TOC
{:toc}

##  Introduction







Summary of Events for 2020:

| Month    | Events                                                                                                                        |
|----------|-------------------------------------------------------------------------------------------------------------------------------|
| Apr 2019 | Hired D&I Manager                                                                                    |
| May 2019 | Incorporated D&I Breakout Sessions at Contribute 2019                            |
| Jun 2019 | Started Monthly D&I Initiatives Call                                     |
| Aug 2019 | All Call for D&I Advisory Group Applications                                                     |
|          | Opened Sign Ups for ERGs (Employee Resource Groups)                                     |
| Sep 2019 | Kickoff Calls for ERGs
|          | GitLab Pride
|          | GitLab MIT - Minorities in Tech
|          | GitLab DiversABILITY
|          | GitLab Women+
| Oct 2019 | Kickoff Call for D&I Advisory Group


