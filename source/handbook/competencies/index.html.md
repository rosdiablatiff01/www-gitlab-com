---
layout: handbook-page-toc
title: "Competencies"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Intro

GitLab has 25 competencies as a common framework. Competences are useful for structuring: job family requirements, interview scoring, promotion criteria, 9 box assessments, high-potential criteria, succession planning, training, PDPs/PIPs, and career planning. The list includes role specific compentencies as well. 

## List of Competencies
1. [Our 6 values](/company/culture/all-remote/values/) 
1. Manager of 1
1. Working async
1. Well written artifacts
1. Single Source of Truth
1. Producing video
1. Handbook first
1. WIP 







